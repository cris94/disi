package util;

/**
 * Created by ovidiuc2 on 3/4/17.
 */
public class Generator {

    public static int[][] generateAllPossibleSolutions(int numberOfItems) {
        final int numberOfSolutions = (int) Math.pow(2, numberOfItems);
        int[][] result = new int[numberOfSolutions][numberOfItems];
        for (int i = 0; i < numberOfSolutions; i++) {
            String binary = convertToBinary(i);
            String binaryComplete = completeWithZeros(binary, numberOfItems);
            int[] intResult = convertToIntVector(binaryComplete);

            for (int j = 0; j < intResult.length; j++) {
                System.out.print(intResult[j] + " ");
                result[i][j] = intResult[j];
            }
            System.out.println();
        }

        return result;
    }

    public static int[] convertToBitArray(int number, int resultSize) {
        String s = convertToBinary(number);
        String ss = completeWithZeros(s, resultSize);
        return convertToIntVector(ss);
    }

    private static String convertToBinary(int number) {
        return Integer.toBinaryString(number);
    }

    private static String completeWithZeros(String binary, int resultSize) {
        String zeroes = "";

        for (int i = 0; i < resultSize - binary.length(); i++) {
            zeroes += "0";
        }

        return zeroes + binary;
    }

    private static int[] convertToIntVector(String resultVector) {
        int[] result = new int[resultVector.length()];
        for (int i = 0; i < resultVector.length(); i++) {
            result[i] = Character.getNumericValue(resultVector.charAt(i));
        }

        return result;
    }
}
