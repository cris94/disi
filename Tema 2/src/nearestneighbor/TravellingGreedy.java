package nearestneighbor;

import model.Coordinate;
import model.TSPInstance;
import reader.FileReader;
import util.TravellingUtil;
import writer.FileWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovidiuc2 on 05.04.2017.
 */
public class TravellingGreedy {

    public static void main(String[] args) {
        FileReader fileReader = new FileReader("inputs/a280.tsp");
        TSPInstance tspInstance = fileReader.readFile();

        int numberOfPoints = tspInstance.getCoordinates().size();
        int lowestCost = Integer.MAX_VALUE;

        long initialTime = System.currentTimeMillis();
        for (int i = 0; i < numberOfPoints; i++) {
            List<Coordinate> points = getListCopy(tspInstance.getCoordinates());
            int cost = TravellingUtil.calculateRouteCost(nearestNeighbour(points, tspInstance.getCoordinates().get(i)));
            lowestCost = lowestCost < cost ? lowestCost : cost;
        }

        int totalTime = (int) (System.currentTimeMillis() - initialTime);
        System.out.println("Lowest cost is " + lowestCost + ". Total time " + totalTime/1000);
        FileWriter.writeGreedyResult(lowestCost, totalTime);
    }

    public static List<Coordinate> nearestNeighbour(List<Coordinate> coordinates, Coordinate startingPoint) {
        List<Coordinate> result = new ArrayList<>();

        Coordinate currentPoint = startingPoint;
        result.add(currentPoint);
        coordinates.remove(currentPoint);

        while (coordinates.size() > 0) {
            int minimumCost = Integer.MAX_VALUE;
            //calculate cost from current point to all points left in the list
            for (int i = 0; i < coordinates.size(); i++) {
                if (TravellingUtil.getCost(currentPoint, coordinates.get(i)) < minimumCost) {
                    minimumCost = TravellingUtil.getCost(currentPoint, coordinates.get(i));
                }
            }

            //find the point which has the minimum cost calculated before
            for (int i = 0; i < coordinates.size(); i++) {
                if (TravellingUtil.getCost(currentPoint, coordinates.get(i)) == minimumCost) {
                    currentPoint = coordinates.get(i);
                    coordinates.remove(coordinates.get(i));
                    result.add(currentPoint);
                }
            }
        }

        return result;
    }

    public static List<Coordinate> getListCopy(List<Coordinate> coordinates) {
        List<Coordinate> result = new ArrayList<>();

        for (Coordinate c : coordinates) {
            result.add(c);
        }

        return result;
    }
}
