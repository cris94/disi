package model;

public class Coordinate {

    private int index;
    private int x;
    private int y;


    public Coordinate(int index) {
        this.index = index;
    }

    public Coordinate(int index, int x, int y) {
        this.index = index;
        this.x = x;
        this.y = y;
    }

    public int getIndex() {
        return index;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /*@Override
    public boolean equals(Object obj) {
        return this.getX() == ((Coordinate) obj).getX() && this.getY() == ((Coordinate) obj).getY();
    }*/
}
