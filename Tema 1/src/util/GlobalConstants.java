package util;

import static filereader.FileReader.NUMBER_OF_OBJECTS;

/**
 * Created by ovidiuc2 on 3/11/17.
 */
public class GlobalConstants {
    public static Integer[] finalResult;
    public static int OPTIMAL_WEIGHT = 0;
    public static int OPTIMAL_VALUE = 0;

    public static final int EXHAUSTIV = 0;
    public static final int RANDOM = 1;
    public static final int GREEDY = 2;

    public static void resetGlobalConstants() {
        finalResult = new Integer[NUMBER_OF_OBJECTS];
        OPTIMAL_WEIGHT = 0;
        OPTIMAL_VALUE = 0;
    }
}
