package model;

import salesmanlibrary.util.TravellingUtil;

import java.util.List;

/**
 * Created by ovidiuc2 on 13.05.2017.
 */
public class TSPRoute implements Comparable {
    private List<Coordinate> route;

    public TSPRoute(List<Coordinate> route) {
        this.route = route;
    }

    public List<Coordinate> getRoute() {
        return route;
    }

    @Override
    public boolean equals(Object obj) {
        List<Coordinate> objCoordinates = ((TSPRoute) obj).getRoute();
        int size = objCoordinates.size();
        for (int i = 0; i < size; i++) {
            if(!route.get(i).equals(objCoordinates.get(i))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int compareTo(Object o) {
        return TravellingUtil.calculateRouteCost(this.getRoute()) - TravellingUtil.calculateRouteCost(((TSPRoute) o).getRoute());
    }
}
