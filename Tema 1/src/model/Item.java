package model;

/**
 * Created by ovidiuc2 on 3/4/17.
 */
public class Item implements Comparable<Item> {

    int index;
    private int value;
    private int weight;
    private double ratio;

    public Item(int index, int value, int weight) {
        this.index = index;
        this.value = value;
        this.weight = weight;
        this.ratio = ((double) value) / weight;
    }

    public int getIndex() {
        return index;
    }

    public int getValue() {
        return value;
    }

    public int getWeight() {
        return weight;
    }

    public double getRatio() {
        return ratio;
    }

    @Override
    public int compareTo(Item o) {
        double ratio = o.getRatio() - this.ratio;

        //if different ratios, return the bigger one
        if (ratio != 0) {
            return (int) (ratio * 100);
        }

        //if equal ratio, return the one with the bigger value
        return o.getValue() - this.getValue();
    }
}
