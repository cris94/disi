package greedy;

import filereader.FileReader;
import filewriter.FileWriter;
import model.Item;
import util.GlobalConstants;

import java.util.Arrays;

import static filereader.FileReader.*;
import static util.GlobalConstants.GREEDY;
import static util.GlobalConstants.finalResult;

/**
 * Created by ovidiuc2 on 3/11/17.
 */
public class Main {

    public static void main(String[] args) {
        FileReader.readFile();
        finalResult = new Integer[NUMBER_OF_OBJECTS];

        long initialTime = System.currentTimeMillis();
        //init final result
        for (int i = 0; i < finalResult.length; i++) {
            finalResult[i] = 0;
        }

        Item[] itemArray = new Item[items.size()];
        int itemCounter = 0;
        for (Item item : items) {
            itemArray[itemCounter] = item;
            itemCounter++;
        }

        Arrays.sort(itemArray);

        int counter = 0;
        int value = 0;
        int weight = 0;
        boolean done = false;
        while (!done) {
            if (counter == itemArray.length) {
                done = true;
            } else if (weight + itemArray[counter].getWeight() <= CAPACITY) {
                finalResult[itemArray[counter].getIndex() - 1] = 1;
                weight += itemArray[counter].getWeight();
                value += itemArray[counter].getValue();
                counter++;
            } else {
                counter++;
            }
        }

        int computationTime = (int) (System.currentTimeMillis()-initialTime)/1000;
        System.out.println("Valoare " + value + " si greutate " + weight);
        FileWriter.writeInFile(finalResult, value, weight, GREEDY, computationTime);
        GlobalConstants.resetGlobalConstants();
    }
}
