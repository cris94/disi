package model;

import java.util.List;

public class TSPInstance {

    private String name;
    private String comment;
    private String type;
    private int dimension;
    private String edgeWeightType;
    private List<Coordinate> coordinates;

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public String getType() {
        return type;
    }

    public int getDimension() {
        return dimension;
    }

    public String getEdgeWeightType() {
        return edgeWeightType;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public void setEdgeWeightType(String edgeWeightType) {
        this.edgeWeightType = edgeWeightType;
    }

    public void setCoordinates(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

}
