package exhaustive;

import model.Coordinate;
import model.TSPInstance;
import reader.FileReader;
import util.TravellingUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovidiuc2 on 04.04.2017.
 */
public class TravellingExhaustive {

    public static void main(String[] args) {
        FileReader fileReader = new FileReader("inputs/test_exh2.tsp");
        long initialTime = System.currentTimeMillis();
        TSPInstance tspInstance = fileReader.readFile();

        List<List<Coordinate>> permutations = Permutations.generatePermutations(tspInstance.getCoordinates());

        List<Integer> costs = new ArrayList<>();
        for (List<Coordinate> c : permutations) {
            Integer cost = TravellingUtil.calculateRouteCost(c);
            System.out.println("Cost " + cost);
            costs.add(cost);
        }

        int totalTime = (int)(System.currentTimeMillis()-initialTime);
        System.out.println("Minimum cost is " + TravellingUtil.getMinimumCost(costs));
        System.out.println("Total time: " + totalTime/1000);
        writer.FileWriter.writeExhaustiveResult(TravellingUtil.getMinimumCost(costs), totalTime/1000, tspInstance.getDimension());
    }
}
