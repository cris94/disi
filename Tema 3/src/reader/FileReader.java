package reader;

import model.Coordinate;
import model.TSPInstance;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileReader {

    private String fileName;

    public FileReader(String fileName) {
        this.fileName = fileName;
    }

    public TSPInstance readFile() {
        TSPInstance instance = new TSPInstance();
        List<String> list = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {

            //br returns as stream and converts it into a List
            list = br.lines().collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        instance.setName(getName(list.get(0)));
        instance.setComment(getComment(list.get(1)));
        instance.setType(getType(list.get(2)));
        instance.setDimension(Integer.parseInt(getDimension(list.get(3))));
        instance.setEdgeWeightType(list.get(4));

        List<Coordinate> coordinates = new ArrayList<>();
        int nrOfLines = list.size();
        for (int i = 6; i < nrOfLines - 1; i++) {
            String line = list.get(i);
            String[] pieces = line.split(" ");
            Coordinate coordinate = new Coordinate(Integer.parseInt(pieces[0]), Integer.parseInt(pieces[1]), Integer.parseInt(pieces[2]));
            coordinates.add(coordinate);
        }

        instance.setCoordinates(coordinates);
        
        /*for(String s: list) {
            System.out.println(s);
        }*/
        return instance;
    }

    private String getName(String rawLine) {
        String[] pieces = rawLine.split("NAME : ");
        return pieces[1];
    }

    private String getComment(String rawLine) {
        String[] pieces = rawLine.split("COMMENT : ");
        return pieces[1];
    }

    private String getType(String rawLine) {
        String[] pieces = rawLine.split("TYPE : ");
        return pieces[1];
    }

    private String getDimension(String rawLine) {
        String[] pieces = rawLine.split("DIMENSION : ");
        return pieces[1];
    }

    private String getEdgeWeightType(String rawLine) {
        String[] pieces = rawLine.split("EDGE_WEIGHT_TYPE : ");
        return pieces[1];
    }

}
