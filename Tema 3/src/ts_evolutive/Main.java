package ts_evolutive;

import model.TSPInstance;
import model.TSPRoute;
import reader.FileReader;
import salesmanlibrary.util.TravellingUtil;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ovidiuc2 on 13.05.2017.
 */
public class Main {

    public static final int INITIAL_POPULATION_SIZE = 50;
    public static final int NUMBER_OF_GENERATIONS = 500;
    public static int TURNIR_SIZE = (int) (0.6 * INITIAL_POPULATION_SIZE);

    public static void main(String[] args) throws URISyntaxException {
        FileReader fileReader = new FileReader("inputs/eil51.tsp");
        TSPInstance tspInstance = fileReader.readFile();

        //initialize population
        List<TSPRoute> initialPopulation = new ArrayList<>();
        for (int i = 0; i < INITIAL_POPULATION_SIZE; i++) {
            TSPRoute tspRoute = new TSPRoute(TravellingUtil.generateRandomPermutation(tspInstance.getCoordinates()));
            initialPopulation.add(tspRoute);
        }

        computeTravellingSalesman(initialPopulation, NUMBER_OF_GENERATIONS);
    }

    public static void computeTravellingSalesman(List<TSPRoute> initialPopulation, int nrOfGenerations) {
        List<TSPRoute> parentPopulation = new ArrayList<>();
        parentPopulation.addAll(initialPopulation);

        for (int counter = 0; counter < nrOfGenerations; counter++) {

            //apply turnir selection
            //for (int i = 0; i < TURNIR_SIZE; i++) {
            Set<TSPRoute> turnirSelected = new HashSet<>();
            while (turnirSelected.size() < TURNIR_SIZE) {
                List<TSPRoute> populationAfterTurnir = EvolutiveUtil.selectTurnirMembers(parentPopulation, 3);
                TSPRoute populationMember = EvolutiveUtil.applyTurnirSelection(populationAfterTurnir);
                //parentPopulation.add(populationMember);
                turnirSelected.add(populationMember);
            }
            //}
            parentPopulation.clear();
            parentPopulation.addAll(turnirSelected);

            Set<TSPRoute> routes = new HashSet<>();
            routes.addAll(parentPopulation);

            //generate children for next generation
            for (int i = 0; i < TURNIR_SIZE; i += 2) {
                //perform crossover between i-th and (i+1)th element
                List<TSPRoute> routesCrossedOver = EvolutiveUtil.performPmxCrossover(parentPopulation.get(i), parentPopulation.get(i + 1));
                parentPopulation.add(routesCrossedOver.get(0));
                parentPopulation.add(routesCrossedOver.get(1));
            }

            //perform mutation for the population
            for (int i = 0; i < 2 * TURNIR_SIZE; i++) {
                parentPopulation.add(EvolutiveUtil.performMutation(parentPopulation.get(i)));
            }

            //select the best INITIAL_POPULATION_SIZE survivors
            //Collections.sort(parentPopulation);
            sortParentPopulation(parentPopulation);
            List<TSPRoute> newGeneration = new ArrayList<>();
            for (int i = 0; i < INITIAL_POPULATION_SIZE; i++) {
                newGeneration.add(parentPopulation.get(i));
            }

            parentPopulation = new ArrayList<>();
            parentPopulation.addAll(newGeneration);
        }

        Set<TSPRoute> routes = new HashSet<>();
        routes.addAll(parentPopulation);

        System.out.println(TravellingUtil.calculateRouteCost(parentPopulation.get(0).getRoute()));
    }

    public static void sortParentPopulation(List<TSPRoute> initialRoute) {
        for (int i = 0; i < initialRoute.size(); i++) {
            for (int j = i + 1; j < initialRoute.size(); j++) {
                if (TravellingUtil.calculateRouteCost(initialRoute.get(i).getRoute()) > TravellingUtil.calculateRouteCost(initialRoute.get(j).getRoute())) {
                    TSPRoute temp = new TSPRoute(initialRoute.get(i).getRoute());
                    initialRoute.set(i, initialRoute.get(j));
                    initialRoute.set(j, temp);
                }
            }
        }
    }
}
