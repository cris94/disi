package main;

import model.Coordinate;
import model.TSPInstance;
import reader.FileReader;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        FileReader fileReader = new FileReader("inputs/eil51.tsp");
        TSPInstance tspInstance = fileReader.readFile();

        System.out.println(tspInstance.getName());
        System.out.println(tspInstance.getComment());
        System.out.println(tspInstance.getType());
        System.out.println(tspInstance.getDimension());
        System.out.println(tspInstance.getEdgeWeightType());

        List<Coordinate> coordinates = tspInstance.getCoordinates();
        for (Coordinate c : coordinates) {
            System.out.println(c.getIndex() + " " + c.getX() + " " + c.getY());
        }
    }

}
