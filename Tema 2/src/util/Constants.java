package util;

/**
 * Created by ovidiuc2 on 15.04.2017.
 */
public class Constants {

    public static final String EXHAUSTIVE = "exhaustive";
    public static final String GREEDY = "greedy";
    public static final String TWO_OPT = "twoOpt";
    public static final String ANNEALING = "annealing";
}
