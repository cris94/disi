package salesmanlibrary.twoopt;

import model.Coordinate;
import model.TSPInstance;
import reader.FileReader;
import salesmanlibrary.util.TravellingUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ovidiuc2 on 05.04.2017.
 */
public class TravellingTwoOpt {

    public static void main(String[] args) {
        FileReader fileReader = new FileReader("inputs/a280.tsp");
        TSPInstance tspInstance = fileReader.readFile();
        int evaluations = 10000;

        //for(int c = 0; c<9;c++) {
        long initialTime = System.currentTimeMillis();
        List<Integer> results = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            List<Coordinate> result = getTwoOptRoute(tspInstance, evaluations);
            results.add(TravellingUtil.calculateRouteCost(result));

            System.out.println("Result is " + TravellingUtil.calculateRouteCost(result));
        }
        int totalTime = (int) ((System.currentTimeMillis() - initialTime));
        System.out.println("Total time " + totalTime);

        //FileWriter.writeTwoOptResult(results, 100, evaluations);
        //}

        /*for (Coordinate c : result) {
            System.out.print(c.getIndex() + " ");
        }*/
    }

    public static List<Coordinate> getTwoOptRoute(TSPInstance tspInstance, int k) {
        List<Coordinate> instanceCopy = getListCopy(tspInstance.getCoordinates());
        List<Coordinate> result = TravellingUtil.generateRandomPermutation(instanceCopy);

        int size = result.size();
        for (int i = 0; i < k; i++) {
            //generate random ii and jj(at least a difference of two between them)
            int ii = new Random().nextInt(size - 3);
            int jj = ThreadLocalRandom.current().nextInt(ii + 3, size + 1);

            //construct the new route
            List<Coordinate> newRoute = new ArrayList<>();
            for (int counter = 0; counter <= ii; counter++) {
                newRoute.add(result.get(counter));
            }

            newRoute.addAll(swap(getCoordinateSublist(result, ii, jj)));

            for (int counter = jj; counter < size; counter++) {
                newRoute.add(result.get(counter));
            }

            if (TravellingUtil.calculateRouteCost(newRoute) < TravellingUtil.calculateRouteCost(result)) {
                result = getListCopy(newRoute);
            }

        }

        return result;
    }

    public static List<Coordinate> swap(List<Coordinate> coordinatesToSwap) {
        List<Coordinate> result = new ArrayList<>();
        int size = coordinatesToSwap.size();
        for (int i = size - 1; i >= 0; i--) {
            result.add(coordinatesToSwap.get(i));
        }

        return result;
    }

    public static List<Coordinate> getCoordinateSublist(List<Coordinate> coordinates, int ii, int jj) {
        List<Coordinate> result = new ArrayList<>();
        for (int i = ii + 1; i < jj; i++) {
            result.add(coordinates.get(i));
        }

        return result;
    }

    public static List<Coordinate> getListCopy(List<Coordinate> coordinates) {
        List<Coordinate> result = new ArrayList<>();

        for (Coordinate c : coordinates) {
            result.add(c);
        }

        return result;
    }
}
