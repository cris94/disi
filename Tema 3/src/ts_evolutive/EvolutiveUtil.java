package ts_evolutive;

import model.Coordinate;
import model.TSPRoute;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ovidiuc2 on 13.05.2017.
 */
public class EvolutiveUtil {

    public static List<TSPRoute> selectTurnirMembers(List<TSPRoute> turnirCandidates, int nrOfMembers) {
        List<TSPRoute> result = new ArrayList<>();

        for (int i = 0; i < nrOfMembers; i++) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, turnirCandidates.size());

            result.add(turnirCandidates.get(randomIndex));
        }

        return result;
    }

    public static TSPRoute applyTurnirSelection(List<TSPRoute> routeMembers) {
        Collections.sort(routeMembers);

        return routeMembers.get(0);
    }

    /**
     * pmx - partially mapped crossover
     *
     * @param route1
     * @param route2
     * @return
     */
    public static List<TSPRoute> performPmxCrossover(TSPRoute route1, TSPRoute route2) {
        List<Coordinate> route1Coordinates = new ArrayList<>();
        route1Coordinates.addAll(route1.getRoute());
        TSPRoute route1Copy = new TSPRoute(route1Coordinates);

        List<Coordinate> route2Coordinates = new ArrayList<>();
        route2Coordinates.addAll(route2.getRoute());
        TSPRoute route2Copy = new TSPRoute(route2Coordinates);

        int routeSize = route1.getRoute().size();


        //1 - choose cut indexes
        //int firstCutIndex = ThreadLocalRandom.current().nextInt(1, routeSize - 1);
        //int secondCutIndex = ThreadLocalRandom.current().nextInt(1, routeSize - 1);
        int firstCutIndex = 4;
        int secondCutIndex = 9;
        while (firstCutIndex == secondCutIndex) {
            secondCutIndex = ThreadLocalRandom.current().nextInt(1, routeSize - 1);
        }

        if (firstCutIndex > secondCutIndex) {
            //swap indexes
            int temp = firstCutIndex;
            firstCutIndex = secondCutIndex;
            secondCutIndex = temp;
        }

        //1.swap between the two indexes
        //2.construct map
        //3.construct set with map elements
        Map<Coordinate, Coordinate> pointMappings = new HashMap<>();
        Set<Integer> mappedPointsSet = new HashSet<>();

        for (int i = firstCutIndex; i <= secondCutIndex; i++) {
            pointMappings.put(route1Copy.getRoute().get(i), route2Copy.getRoute().get(i));
            mappedPointsSet.add(route1Copy.getRoute().get(i).getIndex());
            mappedPointsSet.add(route2Copy.getRoute().get(i).getIndex());

            Coordinate temp = route1Copy.getRoute().get(i);
            route1Copy.getRoute().set(i, route2Copy.getRoute().get(i));
            route2Copy.getRoute().set(i, temp);
        }

        boolean ok = false;
        while (!ok) {
            pointMappings = constructMap(pointMappings);
            List<Coordinate> coordinateList = new ArrayList<>();
            for (Map.Entry<Coordinate, Coordinate> entry : pointMappings.entrySet()) {
                coordinateList.add(entry.getKey());
                coordinateList.add(entry.getValue());
            }

            Set<Integer> checker = new HashSet<>();
            for (Coordinate c : coordinateList) {
                checker.add(c.getIndex());
            }

            if (checker.size() == coordinateList.size()) {
                ok = true;
            }
        }

        //go through the two routes, and update the rest of the elements
        for (int i = 0; i < firstCutIndex; i++) {
            mapElement(route1Copy, pointMappings, mappedPointsSet, i);
        }

        for (int i = secondCutIndex + 1; i < routeSize; i++) {
            mapElement(route1Copy, pointMappings, mappedPointsSet, i);
        }

        for (int i = 0; i < firstCutIndex; i++) {
            mapElement(route2Copy, pointMappings, mappedPointsSet, i);
        }

        for (int i = secondCutIndex + 1; i < routeSize; i++) {
            mapElement(route2Copy, pointMappings, mappedPointsSet, i);
        }

        List<TSPRoute> result = new ArrayList<>();
        result.add(route1Copy);

        Set<Integer> indexes = new HashSet<>();
        for (int i = 0; i < routeSize; i++) {
            indexes.add(route1Copy.getRoute().get(i).getIndex());
        }

        if (indexes.size() != routeSize) {
            //System.out.println("Crossover error");
        }
        result.add(route2Copy);
        return result;
    }

    public static TSPRoute performMutation(TSPRoute route) {
        List<Coordinate> coordinateCopy = new ArrayList<>();
        coordinateCopy.addAll(route.getRoute());
        TSPRoute routeCopy = new TSPRoute(coordinateCopy);

        int randomIndex = ThreadLocalRandom.current().nextInt(0, routeCopy.getRoute().size());

        //swap the randomIndex-th element with the randomIndex-th+1 element
        Coordinate temp = route.getRoute().get(randomIndex);
        int neighbourIndex = randomIndex == routeCopy.getRoute().size() - 1 ? randomIndex - 1 : randomIndex + 1;

        routeCopy.getRoute().set(randomIndex, route.getRoute().get(neighbourIndex));
        routeCopy.getRoute().set(neighbourIndex, temp);

        return routeCopy;
    }

    private static Map<Coordinate, Coordinate> constructMap(Map<Coordinate, Coordinate> initialMap) {
        Map<Coordinate, Coordinate> noKeyValueEqualMap = new ConcurrentHashMap<>();
        for (Map.Entry<Coordinate, Coordinate> entry : initialMap.entrySet()) {
            if (entry.getKey().getIndex() != entry.getValue().getIndex()) {
                noKeyValueEqualMap.put(entry.getKey(), entry.getValue());
            }
        }
        Map<Coordinate, Coordinate> result = new HashMap<>();

        List<Coordinate> coordinates = new ArrayList<>();
        for (Map.Entry<Coordinate, Coordinate> entry : noKeyValueEqualMap.entrySet()) {
            if (entry.getKey().getIndex() != entry.getValue().getIndex()) {
                coordinates.add(entry.getKey());
                coordinates.add(entry.getValue());
            }
        }

        Set<Integer> processed = new HashSet<>();

        for (Map.Entry<Coordinate, Coordinate> entry : noKeyValueEqualMap.entrySet()) {
            if ((!duplicate(entry.getKey(), coordinates) && !duplicate(entry.getValue(), coordinates))) {
                result.put(entry.getKey(), entry.getValue());
            }
        }

        for (Coordinate c : coordinates) {
            if (duplicate(c, coordinates)) {
                if (!processed.contains(c)) {
                    processed.add(c.getIndex());
                    //get key and value
                    Coordinate key = null;
                    Coordinate value = null;

                    for (Map.Entry<Coordinate, Coordinate> entry : noKeyValueEqualMap.entrySet()) {
                        if (entry.getKey().getIndex() == c.getIndex()) {
                            value = entry.getValue();
                            noKeyValueEqualMap.remove(entry.getKey());
                        } else if (entry.getValue().getIndex() == c.getIndex()) {
                            key = entry.getKey();
                            noKeyValueEqualMap.remove(entry.getKey());
                        }
                    }
                    if (key != null && value != null) {
                        noKeyValueEqualMap.put(key, value);
                        result.put(key, value);
                    }
                }
            }
        }

        return result;

    }

    /**
     * @param c
     * @param coordinates
     * @return -true if duplicate
     */
    private static boolean duplicate(Coordinate c, List<Coordinate> coordinates) {
        int count = 0;
        for (Coordinate check : coordinates) {
            if (c.getIndex() == check.getIndex()) {
                count++;
            }
        }

        return count > 1;
    }

    private static void printMap(Map<Coordinate, Coordinate> map) {
        for(Map.Entry<Coordinate, Coordinate> entry: map.entrySet()) {
            System.out.println(entry.getKey().getIndex() + " " + entry.getValue().getIndex());
        }
        System.out.println("\n\n");
    }

    private static void mapElement(TSPRoute route, Map<Coordinate, Coordinate> pointMappings, Set<Integer> mappedPointsSet, int i) {
        if (mappedPointsSet.contains(route.getRoute().get(i).getIndex())) {
            if (containsMyKey(pointMappings, route.getRoute().get(i).getIndex())) {
                //route.getRoute().set(i, pointMappings.get(route.getRoute().get(i)));
                route.getRoute().set(i, getMyValue(pointMappings, route.getRoute().get(i).getIndex()));
            } else {
                for (Map.Entry<Coordinate, Coordinate> point : pointMappings.entrySet()) {
                    int index = point.getValue().getIndex();
                    if (index == route.getRoute().get(i).getIndex()) {
                        route.getRoute().set(i, point.getKey());
                    }
                }
            }
        }
    }

    private static boolean containsMyKey(Map<Coordinate, Coordinate> pointMappings, int keyIndex) {
        for (Map.Entry<Coordinate, Coordinate> entry : pointMappings.entrySet()) {
            if (entry.getKey().getIndex() == keyIndex) {
                return true;
            }
        }

        return false;
    }

    private static Coordinate getMyValue(Map<Coordinate, Coordinate> pointMappings, int keyIndex) {
        for (Map.Entry<Coordinate, Coordinate> entry : pointMappings.entrySet()) {
            if (entry.getKey().getIndex() == keyIndex) {
                return entry.getValue();
            }
        }

        return null;
    }

}
