package filewriter;

import util.BagUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static util.GlobalConstants.*;

/**
 * Created by ovidiuc2 on 3/4/17.
 */
public class FileWriter {

    public static void writeInFile(Integer[] finalSolution, int value, int weight, int problemType, int computationTime) {
        StringBuilder result = new StringBuilder();
        result.append(convertFinalSolutionToString(finalSolution)).append("[value=").append(value).append(", weight=").append(weight)
                .append("];").append(" computation time: ").append(computationTime).append(" seconds.");
        try {
            switch (problemType) {
                case EXHAUSTIV:
                    Files.write(Paths.get("outputs_exhaustive/rucsac-20_" + getUtcTime() + ".txt"), result.toString().getBytes());
                    break;
                case RANDOM:
                    Files.write(Paths.get("outputs_random/rucsac-20" + getUtcTime() + ".txt"), result.toString().getBytes());
                    break;
                case GREEDY:
                    Files.write(Paths.get("outputs_greedy/rucsac-20" + getUtcTime() + ".txt"), result.toString().getBytes());
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeRandomSolutionsInFile(List<String> solutions) {
        StringBuilder stringBuilder = new StringBuilder();
        int counter = 0;
        for (String s : solutions) {
            if (counter % 100 == 0) {
                stringBuilder.append(s).append("\n");
            }
            counter++;
        }

        try {
            Files.write(Paths.get("outputs_random/rucsac-20_" + getUtcTime() + ".txt"), stringBuilder.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeHillClimbingSolutionInFile(int k, int computationTime, int value, int weight, List<Integer> localOptimas, int[] result) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Local optima's: ").append(convertLocalOptimas(localOptimas)).append("\n")
                .append("k= ").append(k)
                .append(", [").append(convertFinalSolutionToString(result)).append("], value=")
                .append(value).append(", weight=")
                .append(weight).append(", computation time ").append(computationTime).append(" milliseconds.");

        try {
            Files.write(Paths.get("outputs_sahc/rucsac-20_" + getUtcTime() + ".txt"), stringBuilder.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static String convertFinalSolutionToString(Integer[] finalSolution) {
        StringBuilder stringBuilder = new StringBuilder();
        final int length = finalSolution.length;
        for (int i = 0; i < length; i++) {
            stringBuilder.append(String.valueOf(finalSolution[i])).append(" ");
        }

        return stringBuilder.toString();
    }

    private static String convertFinalSolutionToString(int[] finalSolution) {
        StringBuilder stringBuilder = new StringBuilder();
        final int length = finalSolution.length;
        for (int i = 0; i < length; i++) {
            stringBuilder.append(String.valueOf(finalSolution[i])).append(" ");
        }

        return stringBuilder.toString();
    }

    private static String getUtcTime() {
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("gmt"));
        return df.format(new Date());
    }

    private static String convertLocalOptimas(List<Integer> localOptimas) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Integer i : localOptimas) {
            stringBuilder.append(i).append(" ");
        }

        return stringBuilder.toString();
    }
}
