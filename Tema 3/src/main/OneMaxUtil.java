package main;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ovidiuc2 on 03.05.2017.
 */
public class OneMaxUtil {

    private OneMaxUtil() {

    }

    /**
     * @param n - number of bits
     * @return
     */
    public static List<Integer> generateRandomArray(int n) {
        List<Integer> result = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            int bit = ThreadLocalRandom.current().nextInt(0, 2);
            result.add(bit);
        }

        return result;
    }

    public static List<List<Integer>> performCrossover(List<Integer> solution1, List<Integer> solution2) {
        List<List<Integer>> result = new ArrayList<>();
        int size = solution1.size();
        int cutIndex = ThreadLocalRandom.current().nextInt(2, solution1.size() - 2);

        //create first child
        List<Integer> child1 = new ArrayList<>();
        for (int i = 0; i < cutIndex; i++) {
            child1.add(solution1.get(i));
        }

        for (int i = cutIndex; i < size; i++) {
            child1.add(solution2.get(i));
        }

        //create the second child

        List<Integer> child2 = new ArrayList<>();
        for (int i = cutIndex; i < size; i++) {
            child2.add(solution1.get(i));
        }

        for (int i = 0; i < cutIndex; i++) {
            child2.add(solution2.get(i));
        }

        result.add(child1);
        result.add(child2);
        return result;
    }

    public static List<Integer> performMutation(List<Integer> member) {
        int memberLength = member.size();

        //generate random number in range[0, memberLength]
        int mutationIndex = ThreadLocalRandom.current().nextInt(0, memberLength);

        int mutationIndexBit = member.get(mutationIndex);
        int newMutationIndexBit = mutationIndexBit == 1 ? 0 : 1;
        member.set(mutationIndex, newMutationIndexBit);

        return member;
    }

    public static List<List<Integer>> selectBestNParents(List<List<Integer>> population, int numberOfSurvivors) {
        Map<List<Integer>, Integer> memberFitnessPair = new HashMap<>();
        List<Candidate> candidates = new ArrayList<>();

        for (List<Integer> member : population) {
            //memberFitnessPair.put(member, getFitnessLevel(member));
            Candidate candidate = new Candidate(member);
            candidates.add(candidate);
        }

        Collections.sort(candidates);

        List<List<Integer>> result = new ArrayList<>();
        int survivorCount = 0;
        for (int i = 0; i < numberOfSurvivors; i++) {
            result.add(candidates.get(i).getCandidate());
        }
        return result;
    }

    public static List<List<Integer>> selectTurnirMembers(List<List<Integer>> turnirCandidates, int nrOfMembers) {
        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < nrOfMembers; i++) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, turnirCandidates.size());

            result.add(turnirCandidates.get(randomIndex));
        }

        return result;
    }

    public static List<Integer> applyTurnirSelection(List<List<Integer>> turnirMembers) {
        List<Candidate> candidates = new ArrayList<>();
        for (List<Integer> turnirMember : turnirMembers) {
            candidates.add(new Candidate(turnirMember));
        }

        Collections.sort(candidates);

        return candidates.get(0).getCandidate();
    }

    public static Integer getFitnessLevel(Candidate candidate) {
        List<Integer> solution = candidate.getCandidate();
        int size = solution.size();
        Integer result = 0;

        for (int i = 0; i < size; i++) {
            if (solution.get(i) == 1) {
                result++;
            }
        }

        return result;
    }

    public static List<List<Integer>> getCopy(List<List<Integer>> init) {
        List<List<Integer>> result = new ArrayList<>();
        for (List<Integer> l : init) {
            result.add(l);
        }

        return result;
    }

    public static List<List<Integer>> selectSurvivors(List<List<Integer>> candidates) {
        List<List<Integer>> survivors = new ArrayList<>();
        return null;
    }

}
