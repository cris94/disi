package exhaustive;

import model.Coordinate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovidiuc2 on 04.04.2017.
 */
public class Permutations {

    public static List<List<Coordinate>> generatePermutations(List<Coordinate> original) {
        if (original.size() == 0) {
            List<List<Coordinate>> result = new ArrayList<>();
            result.add(new ArrayList<>());
            return result;
        }
        Coordinate firstElement = original.remove(0);
        List<List<Coordinate>> returnValue = new ArrayList<>();
        List<List<Coordinate>> permutations = generatePermutations(original);
        for (List<Coordinate> smallerPermutated : permutations) {
            for (int index = 0; index <= smallerPermutated.size(); index++) {
                List<Coordinate> temp = new ArrayList<>(smallerPermutated);
                temp.add(index, firstElement);
                returnValue.add(temp);
            }
        }
        return returnValue;
    }

}
