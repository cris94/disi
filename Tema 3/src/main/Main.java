package main;

import writer.FileWriter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ovidiuc2 on 03.05.2017.
 */
public class Main {

    public static final int N = 10000;
    public static final int NUMBER_OF_GENERATIONS = 1000;
    public static final int INITIAL_POPULATION_SIZE = 64;
    public static int TURNIR_SIZE = (int) (0.6 * INITIAL_POPULATION_SIZE);

    public static void main(String[] args) {
        long initialTime = System.currentTimeMillis();
        if (TURNIR_SIZE % 2 == 1) {
            TURNIR_SIZE++;
        }

        List<List<Integer>> initialPopulation = new ArrayList<>();

        //initialize population
        for (int i = 0; i < INITIAL_POPULATION_SIZE; i++) {
            initialPopulation.add(OneMaxUtil.generateRandomArray(N));
        }

        List<Integer> result = performOneMax(initialPopulation, NUMBER_OF_GENERATIONS);
        System.out.println("Result: " + result + "\n with fitness " + OneMaxUtil.getFitnessLevel(new Candidate(result)));
        int totalTime = (int) ((System.currentTimeMillis() - initialTime)/1000);
        FileWriter.writeOneMaxSolutionInFile(N, NUMBER_OF_GENERATIONS, OneMaxUtil.getFitnessLevel(new Candidate(result)),INITIAL_POPULATION_SIZE, totalTime);
    }

    public static List<Integer> performOneMax(List<List<Integer>> initialPopulation, int noOfIterations) {
        for (int counter = 0; counter < noOfIterations; counter++) {
            List<List<Integer>> allChildren = new ArrayList<>();

            //apply turnir selection
            List<List<Integer>> parentPopulation = new ArrayList<>();
            for (int i = 0; i < TURNIR_SIZE; i++) {
                List<List<Integer>> populationAfterTurnir = OneMaxUtil.selectTurnirMembers(initialPopulation, 3);
                List<Integer> populationMember = OneMaxUtil.applyTurnirSelection(populationAfterTurnir);
                parentPopulation.add(populationMember);
            }
            initialPopulation = OneMaxUtil.getCopy(parentPopulation);

            //generate children for next generation
            for (int i = 0; i < initialPopulation.size(); i += 2) {
                List<Integer> parent1 = initialPopulation.get(i);
                List<Integer> parent2 = initialPopulation.get(i + 1);

                List<List<Integer>> children = OneMaxUtil.performCrossover(parent1, parent2);
                allChildren.addAll(children);
            }

            initialPopulation.addAll(allChildren);

            List<List<Integer>> mutatedPopulation = new ArrayList<>();
            //perform mutation for each member of the initialPopulation
            for (List<Integer> populationMember : initialPopulation) {
                List<Integer> mutatedMember = OneMaxUtil.performMutation(populationMember);
                mutatedPopulation.add(mutatedMember);
            }
            initialPopulation.addAll(mutatedPopulation);

            //select only the best INITIAL_POPULATION_SIZE parents
            initialPopulation = OneMaxUtil.selectBestNParents(initialPopulation, TURNIR_SIZE);
        }

        //select best solution from the survivors
        List<Candidate> candidates = new ArrayList<>();
        for (List<Integer> finalPopulation : initialPopulation) {
            Candidate candidate = new Candidate(finalPopulation);
            candidates.add(candidate);
        }

        Collections.sort(candidates);

        return candidates.get(0).getCandidate();
    }
}
