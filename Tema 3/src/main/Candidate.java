package main;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovidiuc2 on 03.05.2017.
 */
public class Candidate implements Comparable {

    private List<Integer> candidate = new ArrayList<>();

    public Candidate(List<Integer> candidate) {
        this.candidate = candidate;
    }

    public List<Integer> getCandidate() {
        return candidate;
    }

    @Override
    public int compareTo(Object o) {
        return OneMaxUtil.getFitnessLevel((Candidate) o) - OneMaxUtil.getFitnessLevel(this);
    }
}
