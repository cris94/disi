package util;

import model.Coordinate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ovidiuc2 on 04.04.2017.
 */
public class TravellingUtil {

    public static Integer calculateRouteCost(List<Coordinate> coordinates) {
        Integer result = 0;
        int size = coordinates.size();
        for (int i = 0; i < size - 1; i++) {
            result += getCost(coordinates.get(i), coordinates.get(i + 1));
        }

        result += getCost(coordinates.get(size - 1), coordinates.get(0));

        return result;
    }

    public static Integer getMinimumCost(List<Integer> costs) {
        Integer result = Integer.MAX_VALUE;

        for (Integer i : costs) {
            if (i < result) {
                result = i;
            }
        }

        return result;
    }

    public static List<Coordinate> generateRandomPermutation(List<Coordinate> coordinates) {
        List<Coordinate> result = new ArrayList<>();

        while (coordinates.size() > 0) {
            if (coordinates.size() == 1) {
                result.add(coordinates.get(0));
                coordinates.remove(coordinates.get(0));
            } else {
                Coordinate remove = coordinates.get(new Random().nextInt(coordinates.size() - 1));
                result.add(remove);
                coordinates.remove(remove);
            }
        }

        return result;
    }

    public static int cate = 0;
    public static Integer getCost(Coordinate a, Coordinate b) {
        cate++;
        double delta = Math.pow(b.getX() - a.getX(), 2) + Math.pow(b.getY() - a.getY(), 2);
        return (int) Math.round(Math.sqrt(delta));
    }

}
