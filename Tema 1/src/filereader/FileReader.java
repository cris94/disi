package filereader;

import model.Item;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ovidiuc2 on 3/4/17.
 */
public class FileReader {

    public static int NUMBER_OF_OBJECTS;
    public static int CAPACITY;
    public static List<Item> items = new ArrayList<>();

    public static void readFile() {
        String fileName = "inputs/rucsac-20.txt";
        List<String> list = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {

            //br returns as stream and convert it into a List
            list = br.lines().collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                //collect number of objects
                NUMBER_OF_OBJECTS = Integer.parseInt(list.get(0).toString());
            } else if (i == list.size() - 1) {
                //collect capacity
                CAPACITY = Integer.parseInt(list.get(i).toString());
            } else {
                String line = list.get(i);
                String[] info = line.split("   ");

                int index = Integer.parseInt(info[1].trim());
                int value = Integer.parseInt(info[2].trim());
                int weight = Integer.parseInt(info[3].trim());
                items.add(new Item(index, value, weight));
            }


        }

        list.forEach(System.out::println);
    }
}
