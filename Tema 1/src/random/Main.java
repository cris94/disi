package random;

import filereader.FileReader;
import model.Item;
import util.BagUtil;
import util.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static filereader.FileReader.*;
import static util.GlobalConstants.*;

/**
 * Created by ovidiuc2 on 3/8/17.
 */
public class Main {

    private static List<String> results = new ArrayList<>();
    private static List<Integer> solutionValues = new ArrayList<>();

    public static void main(String[] args) {
        FileReader.readFile();
        finalResult = new Integer[NUMBER_OF_OBJECTS];
        randomGenerateSolutions(1000);
    }

    private static void randomGenerateSolutions(int k) {
        int solutionsFound = 0;
        long initialTime = System.currentTimeMillis();
        while (solutionsFound < k) {
            List<Item> trialItems = new ArrayList<>();
            Integer possibleSolution = new Random().nextInt((int) Math.pow(2, NUMBER_OF_OBJECTS));
            int[] bits = Generator.convertToBitArray(possibleSolution, NUMBER_OF_OBJECTS);

            //store the random solution in trialItems variable
            for (int index = 0; index < bits.length; index++) {
                if (bits[index] == 1) {
                    trialItems.add(items.get(index));
                }
            }

            if (BagUtil.isSolutionValid(trialItems, CAPACITY)) {
                solutionsFound++;
                int computationTime = (int) (System.currentTimeMillis() - initialTime);
                initialTime=System.currentTimeMillis();

                addSolutionToList(solutionsFound, trialItems, computationTime);
                if (BagUtil.isSolutionOptimal(trialItems)) {
                    finalResult = BagUtil.convertToIntegerArray(trialItems, NUMBER_OF_OBJECTS);
                    OPTIMAL_WEIGHT = BagUtil.getTotalWeight(trialItems);
                    OPTIMAL_VALUE = BagUtil.getTotalValue(trialItems);
                }
            }
        }


        //System.out.println("Solutia este " + finalResult.toString() + ", cu valoare " + OPTIMAL_VALUE + " si greutate " + OPTIMAL_WEIGHT);
        addConclusionsToFile();
        filewriter.FileWriter.writeRandomSolutionsInFile(results);
        resetGlobalConstants();
    }

    private static void addSolutionToList(int k, List<Item> items, int computationTime) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("k= ").append(k).append(", [").
                append(convertFinalSolutionToString(BagUtil.convertToIntegerArray(items, NUMBER_OF_OBJECTS))).
                append("], value=").append(BagUtil.getTotalValue(items)).append(", weight= ").append(BagUtil.getTotalWeight(items))
                .append(", computationTime=").append(computationTime).append(" milliseconds;");
        results.add(stringBuilder.toString());
        solutionValues.add(BagUtil.getTotalValue(items));
    }

    private static void addConclusionsToFile() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Conclusions:\n").append("best result at:")
                .append(getBestSolution());
        results.add(stringBuilder.toString() + "\n");
        results.add("Average value: " + getAverageValue());

    }

    private static String getBestSolution() {
        int bestResult = 0;
        int bestResultIndex = 0;
        for (int i = 0; i < solutionValues.size(); i++) {
            if (solutionValues.get(i) > bestResult) {
                bestResult = solutionValues.get(i);
                bestResultIndex = i;
            }
        }

        return results.get(bestResultIndex);
    }

    private static String convertFinalSolutionToString(Integer[] finalSolution) {
        StringBuilder stringBuilder = new StringBuilder();
        final int length = finalSolution.length;
        for (int i = 0; i < length; i++) {
            stringBuilder.append(String.valueOf(finalSolution[i])).append(" ");
        }

        return stringBuilder.toString();
    }

    private static int getAverageValue() {
        int valueSum = 0;
        for (int i = 0; i < solutionValues.size(); i++) {
            valueSum += solutionValues.get(i);
        }

        return valueSum / solutionValues.size();
    }
}
