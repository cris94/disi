package annealing;

import model.Coordinate;
import model.TSPInstance;
import reader.FileReader;
import util.TravellingUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ovidiuc2 on 12.04.2017.
 */
public class TravellingAnnealing {

    private static double TMAX = 10000; //temperatura initiala
    private static final int kt = 1000; //numarul de iteratii
    private static final double r = 0.9999; //rata de racire
    private static final double TMIN = 0.00001; //temperatura de inghet

    public static void main(String[] args) {
        FileReader fileReader = new FileReader("inputs/a280.tsp");
        TSPInstance tspInstance = fileReader.readFile();

        long initialTime = System.currentTimeMillis();
        //create random solution
        List<Coordinate> c = TravellingUtil.generateRandomPermutation(tspInstance.getCoordinates());

        int nr = 0;
        while (TMAX > TMIN) {
            nr++;
            for (int i = 0; i < kt; i++) {
                List<Coordinate> x = getNeighbour(c);
                int delta = TravellingUtil.calculateRouteCost(x) - TravellingUtil.calculateRouteCost(c);

                if (delta < 0) {
                    c = getListCopy(x);
                } else {
                    if (ThreadLocalRandom.current().nextDouble(0, 1) < Math.exp(-delta / TMAX)) {
                        c = getListCopy(x);
                    }
                }

            }
            TMAX = r * TMAX;
        }
        int totalTime = (int)((System.currentTimeMillis()-initialTime));

        System.out.println("done: " + TravellingUtil.calculateRouteCost(c) + " in " + totalTime + " milliseconds");
        System.out.println(nr);
        System.out.println("cate " + TravellingUtil.cate);
        //FileWriter.writeAnnealing(TravellingUtil.calculateRouteCost(c), totalTime, kt);
    }

    private static List<Coordinate> getNeighbour(List<Coordinate> route) {
        int size = route.size();

        //generate random ii and jj(at least a difference of two between them)
        int ii = new Random().nextInt(size - 3);
        int jj = ThreadLocalRandom.current().nextInt(ii + 3, size + 1);

        //construct the new route
        List<Coordinate> newRoute = new ArrayList<>();
        for (int counter = 0; counter <= ii; counter++) {
            newRoute.add(route.get(counter));
        }

        newRoute.addAll(swap(getCoordinateSublist(route, ii, jj)));

        for (int counter = jj; counter < size; counter++) {
            newRoute.add(route.get(counter));
        }

        return newRoute;
    }

    public static List<Coordinate> swap(List<Coordinate> coordinatesToSwap) {
        List<Coordinate> result = new ArrayList<>();
        int size = coordinatesToSwap.size();
        for (int i = size - 1; i >= 0; i--) {
            result.add(coordinatesToSwap.get(i));
        }

        return result;
    }

    public static List<Coordinate> getCoordinateSublist(List<Coordinate> coordinates, int ii, int jj) {
        List<Coordinate> result = new ArrayList<>();
        for (int i = ii + 1; i < jj; i++) {
            result.add(coordinates.get(i));
        }

        return result;
    }

    public static List<Coordinate> getListCopy(List<Coordinate> coordinates) {
        List<Coordinate> result = new ArrayList<>();

        for (Coordinate c : coordinates) {
            result.add(c);
        }

        return result;
    }
}
