package writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by ovidiuc2 on 15.04.2017.
 */
public class FileWriter {

    public static void writeOneMaxSolutionInFile(int inputLength, int numberOfGenerations, int fitnessLevel, int initialPopulationSize,  int totalTimeSeconds) {
        StringBuilder output = new StringBuilder();
        output.append("Input length: ").append(inputLength).append(",\nNumber of generations: ").append(numberOfGenerations)
                .append(",\nResult fitness level:").append(fitnessLevel).append(",\nTotal time:")
                .append(totalTimeSeconds).append(" seconds")
                .append(",\nInitial population size: ").append(initialPopulationSize).append("\n\n\n\n");

        try {
            Files.write(Paths.get("outputs_onemax/" + numberOfGenerations + "generations/" + getUtcTime() + ".txt"), output.toString().getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeExhaustiveResult(int solution, int timeSeconds, int numberOfPoints) {
        StringBuilder output = new StringBuilder();
        output.append("Total cost ").append(solution).append(" in a time of ").append(timeSeconds).append(" seconds.");
        output.append("\nNumber of locations: ").append(numberOfPoints).append(".");

        try {
            Files.write(Paths.get("outputs_exhaustive/travelling_" + getUtcTime() + ".txt"), output.toString().getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeGreedyResult(int solution, int timeMillis) {
        String output = "Lowest cost is " + solution + ". Total time " + timeMillis + " milliseconds";
        try {
            Files.write(Paths.get("outputs_greedy/travelling_" + getUtcTime() + ".txt"), output.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeTwoOptResult(List<Integer> solutions, int numberOfRuns, int nrOfEvals) {
        //k should be 1k,5k or 10k
        String outputFilePath = "outputs_twoopt/a280/k" + nrOfEvals + "/";
        int costSum = 0;

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < numberOfRuns; i++) {
            result.append("Lowest cost is " + solutions.get(i)).append("\n");
            costSum += solutions.get(i);
        }

        //calculate average
        result.append("\n\nAverage cost in " + numberOfRuns + " runs is " + costSum / solutions.size());

        try {
            Files.write(Paths.get(outputFilePath + "result" + getUtcTime() + ".txt"), result.toString().getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeAnnealing(int solution, int timeMillis, int nrOfIterations) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Resulting cost: ").append(solution).append(" in ").append(timeMillis)
                .append(" milliseconds.Number of iterations ").append(nrOfIterations);

        try {
            Files.write(Paths.get("outputs_annealing/travelling_" + getUtcTime() + ".txt"), stringBuilder.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private
    static String getUtcTime() {
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("gmt"));
        return df.format(new Date());
    }
}
