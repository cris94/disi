package sahc;

import filereader.FileReader;
import filewriter.FileWriter;
import model.Item;
import util.BagUtil;
import util.Generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static filereader.FileReader.*;

/**
 * Created by ovidiuc2 on 3/15/17.
 */
public class Main {

    public static List<Integer> localOptimas = new ArrayList<>();
    public static int computationTime;

    public static void main(String[] args) {
        FileReader.readFile();
        int k = 500;

        int[] result = steepestAscentHillClimbing(k);

        System.out.println(BagUtil.getTotalValue(convertToItemList(result)));
        FileWriter.writeHillClimbingSolutionInFile(k, computationTime, BagUtil.getTotalValue(convertToItemList(result)),
                BagUtil.getTotalWeight(convertToItemList(result)), localOptimas, result);
    }

    private static int[] getRandomSolution() {
        int[] result = null;
        boolean found = false;

        while (!found) {
            Integer possibleSolution = new Random().nextInt((int) Math.pow(2, NUMBER_OF_OBJECTS));
            result = Generator.convertToBitArray(possibleSolution, NUMBER_OF_OBJECTS);
            List<Item> currentSolution = new ArrayList<>();

            for (int i = 0; i < NUMBER_OF_OBJECTS; i++) {
                if (result[i] == 1) {
                    currentSolution.add(items.get(i));
                }
            }

            if (BagUtil.isSolutionValid(currentSolution, CAPACITY)) {
                found = true;
            }
        }

        return result;
    }

    private static int[][] generateAllNeighbours(int[] currentHilltop) {
        int length = currentHilltop.length;
        int numberOfNeighbours = getNumberOfZeros(currentHilltop);
        int[][] result = new int[numberOfNeighbours][numberOfNeighbours];

        List<Integer> zeroIndexes = getZeroIndexes(currentHilltop);

        for (int i = 0; i < numberOfNeighbours; i++) {
            //system.arraycopy
            //int[] neighbour = currentHilltop;
            int[] neighbour = new int[length];
            for (int j = 0; j < length; j++) {
                neighbour[j] = currentHilltop[j];
            }
            neighbour[zeroIndexes.get(i)] = 1;

            result[i] = neighbour;
        }

        return result;
    }

    private static int getNumberOfZeros(int[] currentHilltop) {
        int result = 0;
        int length = currentHilltop.length;
        for (int i = 0; i < length; i++) {
            if (currentHilltop[i] == 0) {
                result++;
            }
        }

        return result;
    }

    private static List<Integer> getZeroIndexes(int[] solution) {
        int length = solution.length;
        List<Integer> result = new ArrayList<>();

        for (int i = 0; i < length; i++) {
            if (solution[i] == 0) {
                result.add(i);
            }
        }

        return result;
    }

    private static int[] getNextHilltop(int[] currentHilltop, int[][] neighbours) {
        int hilltopValue = BagUtil.getTotalValue(convertToItemList(currentHilltop));
        int numberOfNeighbours = getNumberOfZeros(currentHilltop);
        int currentHilltopValueCopy = BagUtil.getTotalValue(convertToItemList(currentHilltop));
        for (int i = 0; i < numberOfNeighbours; i++) {
            int neighbourWeight = BagUtil.getTotalWeight(convertToItemList(neighbours[i]));
            if (neighbourWeight <= CAPACITY) {
                int neighbourValue = BagUtil.getTotalValue(convertToItemList(neighbours[i]));
                if (neighbourValue > hilltopValue) {
                    hilltopValue = neighbourValue;
                }
            }
        }

        if (hilltopValue == currentHilltopValueCopy) {
            //we are stuck in a local optima
            storeLocalOptima(hilltopValue);
            return null;
        }

        for (int i = 0; i < numberOfNeighbours; i++) {
            if (hilltopValue == BagUtil.getTotalValue(convertToItemList(neighbours[i]))) {
                return neighbours[i];
            }
        }

        return null;
    }

    private static List<Item> convertToItemList(int[] solution) {
        List<Item> result = new ArrayList<>();
        int length = solution.length;

        for (int i = 0; i < length; i++) {
            if (solution[i] == 1) {
                result.add(items.get(i));
            }
        }

        return result;
    }

    private static int[] generateLocalOptima(int[] currentHilltop) {

        //2: Se determina toate punctele x din vecinatatea lui c
        int[][] neighbours = generateAllNeighbours(currentHilltop);

        //3: Daca oricare x are un fitness mai bun decat c atunci c=x ,unde x are cea mai buna valoare eval(x).
        int[] nextHilltop = getNextHilltop(currentHilltop, neighbours);

        return nextHilltop;
    }

    private static int[] steepestAscentHillClimbing(int nrOfEvaluations) {
        long initialTime = System.currentTimeMillis();
        int[] solution = null;
        //1: Se selecteaza un punct aleator c (current hilltop) in spatiul de cautare.
        int[] nextHilltop = getRandomSolution();
        boolean stuck = false;

        for (int i = 0; i < nrOfEvaluations; i++) {
            if (stuck) {
                nextHilltop = getRandomSolution();
                stuck = false;
            } else {
                nextHilltop = generateLocalOptima(nextHilltop);
            }

            //initially solution is null, so we initialize it with nextHilltop
            if (solution == null) {
                solution = nextHilltop;
            }

            //next hilltop is null when we are stuck in a local optima
            if (nextHilltop == null) {
                stuck = true;
                i--;
                System.out.println("stuck");
                continue;
            }

            if (BagUtil.getTotalValue(convertToItemList(nextHilltop)) > BagUtil.getTotalValue(convertToItemList(solution))) {
                solution = nextHilltop;
            }
        }

        computationTime = (int) (System.currentTimeMillis() - initialTime);
        return solution;
    }

    private static void storeLocalOptima(Integer localOptima) {
        localOptimas.add(localOptima);
    }

}
