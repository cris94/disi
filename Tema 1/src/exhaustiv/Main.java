package exhaustiv;

import filereader.FileReader;
import model.Item;
import util.BagUtil;
import util.Generator;

import java.util.ArrayList;
import java.util.List;

import static filereader.FileReader.*;
import static util.GlobalConstants.*;

/**
 * Created by ovidiuc2 on 3/4/17.
 */
public class Main {

    public static void main(String[] args) {
        FileReader.readFile();
        finalResult = new Integer[NUMBER_OF_OBJECTS];
        long initialTime = System.currentTimeMillis();
        int[][] possibleSolutions = Generator.generateAllPossibleSolutions(NUMBER_OF_OBJECTS);

        int nrOfSolutions = (int) Math.pow(2, NUMBER_OF_OBJECTS);

        for (int i = 0; i < nrOfSolutions; i++) {
            List<Item> trialItems = new ArrayList<>();
            for (int j = 0; j < NUMBER_OF_OBJECTS; j++) {
                if (possibleSolutions[i][j] == 1) {
                    trialItems.add(items.get(j));
                }
            }

            if (BagUtil.isSolutionValid(trialItems, CAPACITY)) {
                if (BagUtil.isSolutionOptimal(trialItems)) {
                    finalResult = BagUtil.convertToIntegerArray(trialItems, NUMBER_OF_OBJECTS);
                    OPTIMAL_WEIGHT = BagUtil.getTotalWeight(trialItems);
                    OPTIMAL_VALUE = BagUtil.getTotalValue(trialItems);
                }
            }
        }

        int computationTime = (int) (System.currentTimeMillis() - initialTime)/1000;

        System.out.println("Solutia este " + finalResult.toString() + ", cu valoare " + OPTIMAL_VALUE + " si greutate " + OPTIMAL_WEIGHT);
        filewriter.FileWriter.writeInFile(finalResult, OPTIMAL_VALUE, OPTIMAL_WEIGHT, EXHAUSTIV, computationTime);
        resetGlobalConstants();
    }

}
