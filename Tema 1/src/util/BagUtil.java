package util;

import model.Item;

import java.util.List;

import static util.GlobalConstants.OPTIMAL_VALUE;

/**
 * Created by ovidiuc2 on 3/8/17.
 */
public class BagUtil {

    public static boolean isSolutionValid(List<Item> items, int bagWeight) {
        return getTotalWeight(items) <= bagWeight;
    }

    public static boolean isSolutionOptimal(List<Item> items) {
        return getTotalValue(items) >= OPTIMAL_VALUE;
    }

    public static int getTotalWeight(List<Item> items) {
        int weight = 0;
        for (Item item : items) {
            weight += item.getWeight();
        }

        return weight;
    }

    public static int getTotalValue(List<Item> items) {
        int totalValue = 0;

        for (Item item : items) {
            totalValue += item.getValue();
        }

        return totalValue;
    }

    public static Integer[] convertToIntegerArray(List<Item> items, int nrOfObjects) {
        int size = items.size();
        Integer[] result = new Integer[nrOfObjects];

        for (int i = 0; i < nrOfObjects; i++) {
            result[i] = 0;
        }

        for (int i = 0; i < size; i++) {
            result[items.get(i).getIndex() - 1] = 1;
        }

        return result;
    }
}
